#!/bin/bash

#### Installation de PingCount
### Génération des fichiers de compilation
mkdir cmake-build-release
cd cmake-build-release      # BASE_DIR/ping-count-3/cmake-build-release/
cmake -DCMAKE_BUILD_TYPE=Release -G "Unix Makefiles" ../

### Compilation
make