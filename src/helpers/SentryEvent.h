//
// Created by Simon Mignot on 01/12/17.
//

#ifndef PINGCOUNT_SENTRYEVENT_H
#define PINGCOUNT_SENTRYEVENT_H

#include <string>
#include <ctime>

#include "JSON.h"
#include "JSONValue.h"

#define __FILENAME__ (__builtin_strrchr(__FILE__, '/') ? __builtin_strrchr(__FILE__, '/') + 1 : __FILE__)

class SentryEvent
{
    public:
        explicit SentryEvent(const std::string& message);

        void setTimestamp(time_t timestamp = std::time(nullptr));
        std::string getJSONBody();

        enum class Level { FATAL, ERROR, WARNING, INFO, DEBUG };

        SentryEvent& setLevel(Level level);
        SentryEvent& setCulprit(const std::string& culprit);
        SentryEvent& setRelease(const std::string& release);


    private:
        std::string genUUID();

        JSONObject m_body;

};


#endif //PINGCOUNT_SENTRYEVENT_H
