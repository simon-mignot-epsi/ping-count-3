#include <iostream>
#include <fstream>
#include <pistache/router.h>
#include <pistache/endpoint.h>
#include <csignal>

#include "helpers/SentryEvent.h"
#include "helpers/SentryClient.h"
#include "PingCountAPI.h"

#define CREATE_EVENT(level) SentryEvent("Server started.").setLevel(SentryEvent::Level::level) \
                                                          .setRelease(PINGCOUNT_VERSION) \
                                                          .setCulprit(std::string(__FUNCTION__) + " at " + __FILENAME__ + ":" + std::to_string(__LINE__))

volatile int flag = 1;
void stopServer(int sig)
{
    flag = 0;
}

std::string readDSN(const std::string& filename)
{
    std::ifstream file(filename);
    if(!file)
        return "";
    std::string result;
    std::getline(file, result);
    file.close();
    return result;
}

void startServer(const Pistache::Rest::Router& router, uint16_t port)
{
    Pistache::Http::Endpoint::Options opts = Pistache::Http::Endpoint::options().threads(1);
    Pistache::Address addr(Pistache::Ipv4::any(), Pistache::Port(port));
    Pistache::Http::Endpoint server(addr);
    server.init(opts);
    server.setHandler(router.handler());
    server.serveThreaded();

    SentryClient::getInstance()->sendEvent(CREATE_EVENT(INFO));
    std::cout << "Server listening on port " << port << ".\n";

    //TODO: use thread
    while(flag) { }
}


std::string tryReadDSN()
{
    std::vector<std::string> paths = {"../private.dsn", "./private.dsn", "../public.dsn", "./public.dsn"};
    for(std::vector<std::string>::iterator it = paths.begin(); it != paths.end(); ++it)
    {
        std::string fileContent = readDSN(*it);
        if(!fileContent.empty())
            return fileContent;
    }
    return "";
}


int main()
{
    std::cout << "PingCount build version : " << PINGCOUNT_VERSION << '\n';
    std::srand(static_cast<unsigned int>(std::time(nullptr)));
    SentryClient::configureClient(tryReadDSN());
    uint16_t port = 9080;
    try
    {
        Pistache::Rest::Router router;
        PingCountAPI::setupRoutes(router);

        signal(SIGINT, stopServer);
        signal(SIGTERM, stopServer);
        startServer(router, port);
    }
    catch(std::exception &e)
    {
        SentryClient::getInstance()->sendEvent(CREATE_EVENT(FATAL));
        std::cout << e.what();
    }
    SentryClient::getInstance()->sendEvent(CREATE_EVENT(INFO));
    std::cout << "Shutdown\n";

    return 0;
}
