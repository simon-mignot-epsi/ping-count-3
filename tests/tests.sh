#!/bin/bash

# APP_URL
# APP_PORT

echo "$APP_URL"

# Check response format
echo "# Check response format"
if [[ $(curl ${APP_URL}:${APP_PORT}/ping 2> /dev/null | jq -er 'has("message")') == true \
    && $(curl ${APP_URL}:${APP_PORT}/ping 2> /dev/null | jq -r '.message') == 'pong' ]]
then
    echo "OK /ping"
    curl ${APP_URL}:${APP_PORT}/ping 2> /dev/null | jq -r
else
    echo "KO /ping"
    curl ${APP_URL}:${APP_PORT}/ping 2> /dev/null | jq -r && exit 1
fi

if [[ $(curl ${APP_URL}:${APP_PORT}/count 2> /dev/null | jq -er 'has("pingCount")') ]]
then
    echo "OK /count"
    curl ${APP_URL}:${APP_PORT}/count 2> /dev/null | jq -r
else
    echo "KO /count"
    curl ${APP_URL}:${APP_PORT}/count 2> /dev/null | jq -r && exit 1
fi

# Check incrementation
echo
echo "# Check incrementation"

# Initial count
count=$(curl ${APP_URL}:${APP_PORT}/count 2> /dev/null | jq -r ".pingCount")
echo "initial count = $count"

for i in {1..10}
do
    res=$(curl ${APP_URL}:${APP_PORT}/ping 2> /dev/null)
    res=$(curl ${APP_URL}:${APP_PORT}/count 2> /dev/null | jq -r ".pingCount")
    ((count++))
    echo -n "expected/received : $count/$res"
    if [[ "$count" -ne "$res" ]]
    then
        echo "expected != received"
    else
        echo
    fi
done


